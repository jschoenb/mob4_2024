import {Stack} from "expo-router";

export default function ReviewLayout () {
    return (
        <Stack screenOptions={{
            headerStyle: {
                backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }}
        >
            <Stack.Screen name="index" options={{
                headerTitle: "Reviews",
                title: "Reviews"
            }}/>
            <Stack.Screen name="[id]" options={{
                headerTitle: "Review Details",
                title: "Review Details"
            }}/>
        </Stack>
    )
}