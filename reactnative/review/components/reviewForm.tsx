import { Button, TextInput, View, Text } from 'react-native';
import { globalStyles } from '../styles/global';
import { Formik } from 'formik';
import {Review} from "../app/(reviews)";
import * as yup from 'yup';
import FlatButton from "./button";

type ReviewFormProps = {
    addReview: (review: Review) => void;
}

const reviewSchema = yup.object({
    title: yup.string()
        .required()
        .min(4),
    body: yup.string()
        .required()
        .min(8),
    rating: yup.string()
        .required()
        .test('is-num-1-5', 'Rating must be a number 1 - 5', (val) => {
            return parseInt(val) < 6 && parseInt(val) > 0;
        }),
});


export default function ReviewForm(props: ReviewFormProps) {
    return (
        <View style={globalStyles.container}>
            <Formik
                validationSchema={reviewSchema}
                initialValues={{ title: '', body: '', rating: 0, key: ''}}
                onSubmit={(values:Review, actions) => {
                    actions.resetForm();
                    props.addReview(values);
                }}
            >
                {(props) => (
                    <View>
                        <TextInput
                            style={globalStyles.input}
                            placeholder='Review title'
                            onChangeText={props.handleChange('title')}
                            value={props.values.title}
                        />
                        <Text style={globalStyles.errorText}>{props.touched.title && props.errors.title}</Text>
                        <TextInput
                            style={globalStyles.input}
                            multiline
                            placeholder='Review details'
                            onChangeText={props.handleChange('body')}
                            value={props.values.body}
                        />
                        <Text style={globalStyles.errorText}>{props.touched.body && props.errors.body}</Text>
                        <TextInput
                            style={globalStyles.input}
                            placeholder='Rating (1 - 5)'
                            onChangeText={props.handleChange('rating')}
                            value={props. values.rating.toString()}
                            keyboardType='numeric'
                        />
                        <Text style={globalStyles.errorText}>{props.touched.rating && props.errors.rating}</Text>
                        <FlatButton onPress={props.handleSubmit} text='Submit' />
                    </View>
                )}
            </Formik>
        </View>
    );
}
