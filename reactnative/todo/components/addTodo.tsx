import {TextInput, StyleSheet, Button, View} from "react-native";
import {useState} from "react";

type AddTodoProps = {
    submitHandler: (text:string) => void;
}

export default function AddTodo(props:AddTodoProps){
    const [text, setText] = useState('');

    return (
        <View>
            <TextInput style={styles.input}
               placeholder={'new todo...'}
               value={text}
               onChangeText={setText}
            />
            <Button title={'Add Todo'} color='coral' onPress={()=>props.submitHandler(text)}/>
        </View>
    );
}

const styles = StyleSheet.create({
    input: {
        marginBottom: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#ddd',
    },
});