import {
  FlatList, Keyboard,
  SafeAreaView,
  StyleSheet,
  TouchableWithoutFeedback,
  View
} from 'react-native';
import {useState} from "react";
import Header from "./components/header";
import TodoItem from "./components/todoItem";
import AddTodo from "./components/addTodo";

export default function App() {

  const [todos, setTodos] = useState([
    { text: 'learn programming', key: '1' },
    { text: 'do some project work', key: '2' },
    { text: 'party on the weekend', key: '3' },
  ])


  const pressHandler = (key:string) =>{
    setTodos((prevState)=>{
      return prevState.filter(todo => todo.key != key)
    });
  }

  const submitHandler = (text:string) =>{
    setTodos(prevTodods =>{
        return [
          ...prevTodods,
            {text:text, key:Math.random().toString()}

        ];
    });
  }

  return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        <SafeAreaView style={{flex:1}}>
          <View style={styles.container}>
            {/* header */}
            <Header/>
            <View style={styles.content}>
              <AddTodo submitHandler={submitHandler}/>
              <View style = {styles.list}>
                <FlatList data={todos} renderItem={
                  ({item})=>(
                      <TodoItem item={item} pressHandler={pressHandler}/>
                  )
                }/>
              </View>
            </View>
          </View>
          </SafeAreaView>
      </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  content: {
    padding: 40,
  },
  list: {
      marginTop: 20,
  },
});
