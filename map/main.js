function initMap(){
    navigator.geolocation.getCurrentPosition((position)=>{
        let latitude = position.coords.latitude;
        let longitude = position.coords.longitude;

        let coords = {lat:latitude, lng:longitude};
        let map = new google.maps.Map(document.querySelector("#map"),{
            center: coords,
            zoom: 15
        });

        let marker = new google.maps.Marker({
            position: coords,
            map: map,
            title: "I am here!"
        })
    })
}
