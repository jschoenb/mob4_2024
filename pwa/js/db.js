
// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.1/firebase-app.js";
import { getFirestore, collection, addDoc,deleteDoc, doc,onSnapshot, enableIndexedDbPersistence } from "https://www.gstatic.com/firebasejs/10.12.1/firebase-firestore.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
apiKey: "AIzaSyAKfx3I6VHjOhMfEFWwQJVC2ToJoQsKc-k",
authDomain: "pwa2024g1.firebaseapp.com",
projectId: "pwa2024g1",
storageBucket: "pwa2024g1.appspot.com",
messagingSenderId: "58569583239",
appId: "1:58569583239:web:27334487e0f5a3daaa2622"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

enableIndexedDbPersistence(db).catch((err) => {
    if (err.code == 'failed-precondition') {
        // Multiple tabs open, persistence can only be enabled
        // in one tab at a a time.
        // ...
        console.log("Multiple tabs open");
    } else if (err.code == 'unimplemented') {
        // The current browser does not support all of the
        // features required to enable persistence
        // ...
        console.error("browser does not support required features");
    }
});


const form = document.querySelector('form');
form.addEventListener('submit',async (ev)=>{
   ev.preventDefault();
   const contact = {
       name: form.title.value,
       contactnumbers : form.numbers.value
   }
   await addDoc(collection(db,'contacts'),contact);
   form.title.value = "";
   form.numbers.value = "";
});

const contactContainer = document.querySelector('.contacts');
contactContainer.addEventListener('click', async (ev)=>{
   if(ev.target.tagName ==="I"){
       const id = ev.target.getAttribute('data-id');
       if(id){
           await deleteDoc(doc(db,'contacts',id));
       }
   }
});

onSnapshot(collection(db,'contacts'),(snapshot)=>{
   snapshot.docChanges().forEach(change =>{
        if(change.type ==="added"){
            renderContact(change.doc.data(),change.doc.id);
        } else if(change.type ==="removed"){
            removeContact(change.doc.id);
        }
   });
});