document.addEventListener('DOMContentLoaded', function() {
  // nav menu
  const menus = document.querySelectorAll('.side-menu');
  M.Sidenav.init(menus, {edge: 'left'});
  // add recipe form
  const forms = document.querySelectorAll('.side-form');
  M.Sidenav.init(forms, {edge: 'right'});
});

function renderContact(data, id) {
  const html = `
  <div class="grey-text text-darken-1 kwm-contact">
      <div class="contact-image">
        <img src="img/kwmcontacts.png" alt="contact thumb">
      </div>
      <div class="contact-details">
        <div class="contact-title">${data.name}</div>
        <div class="contact-numbers">${data.contactnumber}</div>
      </div>
      <div class="contact-options">        
        <i class="material-icons">call</i>              
        <i class="material-icons" data-id="${id}">delete_outline</i>
      </div>
    </div>`;

  const contacts = document.querySelector('.contacts');
  contacts.insertAdjacentHTML("beforeend", html);
}

function removeContact(id){
  const icon = document.querySelector("[data-id='"+id+"']");
  let contactToRemove = icon.closest(".kwm-contact");
  contactToRemove.remove();
}