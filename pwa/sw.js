const CACHE_NAME = "kwm-cache-v3";
const CACHED_URLS_SERVER = [
    "css/materialize.min.css",
    "css/styles.css",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "img/kwmcontacts.png",
    "js/app.js",
    "js/common.js",
    "pages/about.html",
    "index.html",
    "/"
];

const CACHED_URLS = [
    "/pwa/css/materialize.min.css",
    "/pwa/css/styles.css",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "/pwa/img/kwmcontacts.png",
    "/pwa/js/app.js",
    "/pwa/js/common.js",
    "/pwa/pages/about.html",
    "/pwa/index.html",
    "/pwa/"
];

self.addEventListener("install", function(event) {
    console.log("install");
    event.waitUntil(
        caches.open(CACHE_NAME).then(function(c) {
            return c.addAll(CACHED_URLS);
        }).catch((err)=>{
            console.error(err);
        })
    );
})

self.addEventListener("fetch", function(event) {
    event.respondWith(
        fetch(event.request).catch(function() {
            return caches.match(event.request).then(function(response) {
                return response;
            });
        })
    );
});

self.addEventListener("activate", (ev)=>{
    ev.waitUntil(
      caches.keys().then((cacheNames)=>{
        return Promise.all(
            cacheNames.map((cacheName)=>{
                if(cacheName !== CACHE_NAME && cacheName.startsWith("kwm-cache")){
                    return caches.delete(cacheName);
                }
            })
        )
      })
    );
});